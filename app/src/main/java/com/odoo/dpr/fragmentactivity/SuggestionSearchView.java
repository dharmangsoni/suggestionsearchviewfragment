package com.odoo.dpr.fragmentactivity;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Rect;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.AutoCompleteTextView;

import java.util.Timer;
import java.util.TimerTask;

public class SuggestionSearchView extends SearchView {

    private CursorAdapter cursorAdapter;
    private SearchViewActionListener mSearchViewActionListener;
    private Timer timer = new Timer();

    public SuggestionSearchView(Context context) {
        super(context);
    }

    public SuggestionSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SuggestionSearchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setSearchViewActionListener(SearchViewActionListener callback) {
        mSearchViewActionListener = callback;
    }

    public void setAdapterItems(String[] from, @IdRes int[] to, @LayoutRes int layout) {
        cursorAdapter = new SimpleCursorAdapter(getContext(), layout, null, from, to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        setSuggestionsAdapter(cursorAdapter);
        setOnSuggestionListener(suggestionListener);
        setOnQueryTextListener(queryTextListener);

        final AutoCompleteTextView searchEditText = (AutoCompleteTextView) findViewById(R.id.search_src_text);
        final View dropDown = findViewById(searchEditText.getDropDownAnchor());
        if (dropDown != null) {
            dropDown.addOnLayoutChangeListener(new OnLayoutChangeListener() {
                @Override
                public void onLayoutChange(View v, int left, int top, int right, int bottom,
                                           int oldLeft, int oldTop, int oldRight, int oldBottom) {

                    int point[] = new int[2];
                    dropDown.getLocationOnScreen(point);
                    Rect screenSize = new Rect();
                    Display display = ((WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE))
                            .getDefaultDisplay();
                    display.getRectSize(screenSize);
                    int screenWidth = screenSize.width();
                    searchEditText.setDropDownWidth(screenWidth * 2);
                }
            });
        }

    }

    private OnSuggestionListener suggestionListener = new OnSuggestionListener() {
        @Override
        public boolean onSuggestionSelect(int position) {
            if (mSearchViewActionListener != null) {
                return mSearchViewActionListener.OnSuggestionItemClick(position,
                        (Cursor) cursorAdapter.getItem(position));
            }
            return false;
        }

        @Override
        public boolean onSuggestionClick(int position) {
            if (mSearchViewActionListener != null) {
                return mSearchViewActionListener.OnSuggestionItemClick(position,
                        (Cursor) cursorAdapter.getItem(position));
            }
            return false;
        }
    };

    private OnQueryTextListener queryTextListener = new OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(final String newText) {
            if (mSearchViewActionListener != null) {
                timer.cancel();
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (newText.trim().isEmpty()) {
                            cursorAdapter.changeCursor(null);
                            return;
                        }
                        final Cursor cr = mSearchViewActionListener.OnSearchTextChange(newText);
                        if (cr != null) {
                            post(new Runnable() {
                                @Override
                                public void run() {
                                    cursorAdapter.changeCursor(cr);
                                }
                            });
                        }
                    }
                }, 350);
            }
            return false;
        }
    };

    public interface SearchViewActionListener {

        Cursor OnSearchTextChange(String query);

        boolean OnSuggestionItemClick(int position, Cursor item);
    }
}