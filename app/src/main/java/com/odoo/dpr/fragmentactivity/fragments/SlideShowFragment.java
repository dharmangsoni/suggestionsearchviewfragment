package com.odoo.dpr.fragmentactivity.fragments;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.odoo.dpr.fragmentactivity.R;
import com.odoo.dpr.fragmentactivity.SuggestionSearchView;

/**
 * Created by dpr on 25/7/16.
 */
public class SlideShowFragment extends Fragment implements SuggestionSearchView.SearchViewActionListener {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.content_main, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);


        // get menu from activity and bind it
        SuggestionSearchView searchView = (SuggestionSearchView) MenuItemCompat
                .getActionView(menu.findItem(R.id.menu_search));
        searchView.setSearchViewActionListener(this);
        String[] from = {"name"};
        int[] to = {android.R.id.text1};
        searchView.setAdapterItems(from, to, android.R.layout.simple_list_item_1);
    }

    @Override
    public Cursor OnSearchTextChange(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{BaseColumns._ID, "name"});
        for (int i = 1; i <= 5; i++) {
            c.addRow(new Object[]{i, query + " " + i});
        }
        return c;
    }

    @Override
    public boolean OnSuggestionItemClick(int position, Cursor item) {
        return false;
    }
}
